//--Current Status--
//This file checks the current status of all coins
//Output is JSON object

//include the model (aka DB connection)
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'crypto-signal-scanner.cretxa1wzey9.us-west-1.rds.amazonaws.com',
  user     : 'c01nscan',
  password : '0maa2WUZUX9m',
  database : 'cryptoscannerDB'
});
// console.log(connection);
exports.handler = (event, context, callback) => {

  //query the DB
  let coin =  event.pathParameters.proxy;
  let timeframe = event.queryStringParameters.timeframe;
  let pair = ''
  if(coin=="BTC"){
    pair = coin + '/USDT';
  } else {
    pair = coin + '/BTC';
  }

  var query = "SELECT coinData.*,TRUNCATE((((coinData.price - coinData.foundPrice) / coinData.foundPrice) * 100),2) AS pctg,DATE_FORMAT(coinData.foundTime, '%Y-%m-%d %H:%i') AS foundTime,coins.coin FROM coinData INNER JOIN coins ON SUBSTRING_INDEX(coinData.pair, '/', 1) = coins.ticker WHERE pair = ? AND timeFrame = ?";

    connection.query(query, [pair,timeframe], function (error, results, fields) {
        if (error) {
            connection.destroy();
            throw error;
        } else {
          // connected!
          var response = {
            statusCode: 200,
            headers: {
              'Access-Control-Allow-Origin': '*'
            },
            "body": JSON.stringify(results),
            "isBase64Encoded": false
          };

          callback(error, response);
          //connection.end(function (err) { callback(err, results);});

          connection.end( function(err) {
            if (err) {console.log("Error ending the connection:",err);}

             //  reconnect in order to prevent the"Cannot enqueue Handshake after invoking quit"
            connection = mysql.createConnection({
              host     : 'crypto-signal-scanner.cretxa1wzey9.us-west-1.rds.amazonaws.com',
              user     : 'c01nscan',
              password : '0maa2WUZUX9m',
              database : 'cryptoscannerDB'
            });

            callback(null, {
              statusCode: 200,
              body: response,
            });//End callback
          });//End connecion.end
        }//End if(error)
    });//End connection.query
};//End exports.handler
